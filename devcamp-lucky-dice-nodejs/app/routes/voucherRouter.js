// Import bộ thư viện express
const express = require('express'); 

//Import middleware
const luckydiceMiddleware = require('../middlewares/luckydiceMiddleware')

//Tạo 1 instance router
const router = express.Router();

//Import controller
const {
  createVoucher,
  getAllVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
} = require('../controllers/voucherController');

router.use('/', luckydiceMiddleware);

router.post('/vouchers', createVoucher);

router.get('/vouchers', getAllVoucher);

router.get('/vouchers/:voucherId', getVoucherById);

router.put('/vouchers/:voucherId', updateVoucherById);

router.delete('/vouchers/:voucherId', deleteVoucherById);

module.exports = router;