const express = require('express')
const router = express.Router()

router.use('/', (req, res, next) => {
  console.log(`Time: ${new Date()}`);
  next();
}, (req, res, next) => {
  console.log(`Method: ${req.method}`);
  next();
})

module.exports = router;