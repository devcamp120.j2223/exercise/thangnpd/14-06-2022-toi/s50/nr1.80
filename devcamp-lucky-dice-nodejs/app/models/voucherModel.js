// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const voucherSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  code: {
    type: String, 
    unique: true, 
    required: true
  },
	discount: {
    type:Number, 
    required: true
  },
	note: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date, 
    default: Date.now()
  }
})

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("voucher", voucherSchema);