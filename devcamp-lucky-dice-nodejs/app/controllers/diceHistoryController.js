//Import model dice history
const diceHistoryModel = require('../models/diceHistoryModel');

// Khai báo thư viện mongoose để tạo _id
const mongoose = require("mongoose");

const createDiceHistory = (request, response) => {
  // B1: Thu thập dữ liệu
  let userId = request.params.userId;
  let bodyRequest = request.body;

  // B2: Kiểm tra dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "userId is required"
    })
  }

  if (!bodyRequest.dice) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "dice is required"
    })
  }

  // B3: Thao tác với cơ sở dữ liệu
  let createDiceHistory = {
    _id: new mongoose.Types.ObjectId(),
    user: userId,
    dice: bodyRequest.dice
  }

  diceHistoryModel.create(createDiceHistory, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(201).json({
        status: "Success: Dice history created",
        data: data
      })
    }
  })
}

const getAllDiceHistory = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.find((error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get all dice history success",
        data: data
      })
    }
  })
}

const getDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let diceHistoryId = request.params.diceHistoryId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "diceHistoryId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.findById(diceHistoryId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get dice history by ID success",
        data: data
      })
    }
  })
}

const updateDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let diceHistoryId = request.params.diceHistoryId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "diceHistoryId is not valid"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(bodyRequest.user)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "user ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let diceHistoryUpdate = {
    user: bodyRequest.user,
    dice: bodyRequest.dice
  }

  diceHistoryModel.findByIdAndUpdate(diceHistoryId, diceHistoryUpdate, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Update dice history by ID success",
        data: data
      })
    }
  })
}

const deleteDiceHistoryById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let diceHistoryId = request.params.diceHistoryId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "diceHistoryId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(204).json({
        status: "Success: Delete dice history success"
      })
    }
  })
}

module.exports = {
  createDiceHistory,
  getAllDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById
}