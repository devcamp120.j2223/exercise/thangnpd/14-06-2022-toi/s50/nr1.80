//Import voucher model vào
const voucherModel = require('../models/voucherModel');

// Khai báo thư viện mongoose để tạo _id
const mongoose = require("mongoose");

const createVoucher = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.code) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "code is required"
    })
  }
  if (!(Number.isInteger(bodyRequest.discount) && bodyRequest.discount > 0)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "discount is not valid"
    })
  }

  // B3: Thao tác với cơ sở dữ liệu
  let createVoucher = {
    _id: mongoose.Types.ObjectId(),
    code: bodyRequest.code,
    discount: bodyRequest.discount,
    note: bodyRequest.note
  }

  voucherModel.create(createVoucher, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(201).json({
        status: "Success: Voucher created",
        data: data
      })
    }
  })
}

const getAllVoucher = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.find((error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get all vouchers success",
        data: data
      })
    }
  })
}

const getVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let voucherId = request.params.voucherId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "voucherId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.findById(voucherId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get voucher by ID success",
        data: data
      })
    }
  })
}

const updateVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let voucherId = request.params.voucherId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "voucherId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let voucherUpdate = {
    code: bodyRequest.code,
    discount: bodyRequest.discount,
    note: bodyRequest.note
  }

  voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Update voucher by ID success",
        data: data
      })
    }
  })
}

const deleteVoucherById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let voucherId = request.params.voucherId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(voucherId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "voucherId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  voucherModel.findByIdAndDelete(voucherId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(204).json({
        status: "Success: Delete voucher success"
      })
    }
  })
}

module.exports = {
  createVoucher,
  getAllVoucher,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById
}