const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

// Import mongooseJS and create uri
const mongoose = require("mongoose");
const uri = "mongodb://localhost:27017/CRUD_LuckyDice";


//Import model
const userModel = require('./app/models/userModel');
const dicehistoryModel = require('./app/models/diceHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const voucherModel = require('./app/models/voucherModel');

//Import router
const userRouter = require('./app/routes/userRouter');
const diceHistoryRouter = require('./app/routes/diceHistoryRouter');
const prizeRouter = require('./app/routes/prizeRouter');
const voucherRouter = require('./app/routes/voucherRouter');

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//Use router
app.use('/', userRouter);
app.use('/', diceHistoryRouter);
app.use('/', prizeRouter);
app.use('/', voucherRouter);

app.use(express.static(`views/lucky_dice`)); // Use this for show image

//trả ra file giao diện lucky dice casino
app.get('/', (req, res) => {
  console.log(`__dirname: ${__dirname}`);
  res.sendFile(path.join(`${__dirname}/views/lucky_dice/31.30.html`));
})

//Return random number from 1 to 6 
app.get('/random-number', (req, res) => {
  let randomNum = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
  res.json({
    randomNum: randomNum
  });
})

mongoose.connect(uri, function (error) { // Connect to DB Mongo
	if (error) throw error;
	console.log('MongoDB Successfully connected');
})


app.listen(port, () => {
  console.log(`NR1.80 app listening on port ${port}`);
})